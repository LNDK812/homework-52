import React, { Component } from 'react';
import Number from './components/Number';
import './App.css';

class App extends Component {

    state = {
        numbers: []
    };

  clickHandler = () => {
      let numbersArray = this.generateRandomNumbers();
      let stateNumbers = [...this.state.numbers];
      stateNumbers = numbersArray;
      this.setState({
          numbers: stateNumbers.sort((a,b) => a - b)
      })
  };

  generateRandomNumbers  = () => {
    let array = [];
    while(array.length <= 5) {
        const randNum = Math.floor(Math.random() * 32 + 5);
        if(!array.includes(randNum)){
            array.push(randNum);
        }
    }
    return array;
  };

  render() {
    return (
        <div>
            {this.state.numbers.map((number) => {
                return (
                    <Number text={number}/>
                )
            })}
          <button onClick={this.clickHandler}>
            click
          </button>
        </div>
    );
  }
}

export default App;
