import React from 'react';

const Number = (props) => {
    return (
        <div className='num'>
            <span>{props.text}</span>
        </div>
    )
};


export default Number;